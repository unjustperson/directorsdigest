const TommyArray = [];

export default function fillInstanceTommies() {
  for (let i = 0; i < 5; i++) {
    TommyArray[i] = {
      biography: 'One of the greatest',
      id: 9999 + i,
      name: 'Tommy Wiseau',
      photo: '../assets/images/tommy.png',
    };
  }
  return TommyArray;
}
