const filmXml = `
<svg width="92" height="64" viewBox="0 0 92 64" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M84.3333 39.9695V23.9817C84.3333 10.6585 76.6666 5.32927 57.5 5.32927H34.5C15.3333 5.32927 7.66663 10.6585 7.66663 23.9817V39.9695C7.66663 53.2927 15.3333 58.622 34.5 58.622H57.5C76.6666 58.622 84.3333 53.2927 84.3333 39.9695Z" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.66003 45.5919H82.34" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.66003 18.9456H82.34" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M26.7184 45.5919V57.1831" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M46 45.5919V58.542" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M65.0516 45.5919V57.3429" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M26.7184 5.62238V17.2135" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M46 5.62238V18.5725" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M46 18.7324V48.0434" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M65.0516 5.62238V17.3734" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg> `;

export default filmXml;
