const directorXml = `
<svg width="92" height="57" viewBox="0 0 92 57" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M29.8378 28.5C36.7041 28.5 42.2703 23.1834 42.2703 16.625C42.2703 10.0666 36.7041 4.75 29.8378 4.75C22.9716 4.75 17.4054 10.0666 17.4054 16.625C17.4054 23.1834 22.9716 28.5 29.8378 28.5Z" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M51.1967 52.25C51.1967 43.0588 41.6237 35.625 29.8378 35.625C18.0518 35.625 8.47888 43.0588 8.47888 52.25" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M70.61 41.1231H58.8241C52.9311 41.1231 50.973 37.3825 50.973 33.6241V18.6259C50.973 12.9972 52.9311 11.1269 58.8241 11.1269H70.61C76.503 11.1269 78.4611 12.9972 78.4611 18.6259V33.6241C78.4611 39.2528 76.4844 41.1231 70.61 41.1231Z" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M83.6455 35.2094L78.4612 31.7359V20.4962L83.6455 17.0228C86.1817 15.3306 88.2704 16.3637 88.2704 19.3384V32.9116C88.2704 35.8862 86.1817 36.9194 83.6455 35.2094Z" stroke="#5E3F3D" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
`;

export default directorXml;
