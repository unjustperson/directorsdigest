const RoomArray = [];

function fillInstanceRooms() {
  for (let i = 0; i < 5; i++) {
    RoomArray[i] = {
      biography: 'One of the greatest',
      id: 9999 + i,
      film_name: 'The Room',
      photo: '../assets/images/tommy.png',
      director: 'Tommy Wiseau',
      screenplay: 'Tommy Wiseau',
      year: 2003,
      film_photo: '../assets/images/the_room_still.png',
      film_logo: '../assets/images/the_room_logo.png',
      trailer_link: 'x5s3t28',
      film_synopsis:
        "Johnny is a successful bank executive who lives quietly in a San Francisco townhouse with his fiancée, Lisa. One day, putting aside any scruple, she seduces Johnny's best friend, Mark. From there, nothing will be the same again.",
    };
  }
  return RoomArray;
}

fillInstanceRooms();

export default RoomArray;
