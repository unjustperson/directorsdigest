/* eslint-disable no-undef */
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Alphabet from './screens/Alphabet';
import Baroque from './screens/Baroque';
import Celsius from './screens/Celsius';
import Home from './screens/Home';
import React from 'react';
import DalvikListContainer from './screens/Dalvik';
import Elk from './screens/elk_screens/Elk';
import Film from './screens/elk_screens/Film';
import Directors from './screens/elk_screens/Directors';
import DirectorFilmography from './screens/elk_screens/DirectorFilmography';
import FilmDetail from './screens/elk_screens/FilmDetail';
import Fun from './screens/fun_screens/Fun';

const Stack = createStackNavigator();

export default App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Home">
        <Stack.Screen component={Home} name="Home" />
        <Stack.Screen component={Alphabet} name="Alphabet" />
        <Stack.Screen component={Baroque} name="Baroque" />
        <Stack.Screen component={Celsius} name="Celsius" />
        <Stack.Screen component={DalvikListContainer} name="Dalvik" />
        <Stack.Screen component={Elk} name="Elk" />
        <Stack.Screen component={Directors} name="Directors" />
        <Stack.Screen component={Film} name="Films" />
        <Stack.Screen
          component={DirectorFilmography}
          name="DirectorFilmography"
        />
        <Stack.Screen component={FilmDetail} name="FilmDetail" />
        <Stack.Screen component={Fun} name="Fun" />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
