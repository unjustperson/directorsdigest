import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {ImageBackground, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import filmStyles from '../styles/filmStyles';
ImageBackground;

const FilmComponent = ({film, error}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={filmStyles.container}
      onPress={() => {
        !error ? navigation.navigate('FilmDetail', {film: film}) : {};
      }}>
      <ImageBackground
        style={filmStyles.image}
        source={
          error
            ? require('../../assets/images/the_room_still.png')
            : {uri: film.film_photo}
        }>
        <Image
          style={filmStyles.logoImage}
          source={
            error
              ? require('../../assets/images/the_room_logo.png')
              : {uri: film.film_logo}
          }
        />
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default FilmComponent;
