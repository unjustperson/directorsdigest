import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {ImageBackground, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import directorsStyles from '../styles/directorsStyles';

/* eslint-disable react/react-in-jsx-scope */

const DirectorComponent = ({director, error}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={directorsStyles.container}
      onPress={() => {
        navigation.navigate('DirectorFilmography', {director: director});
      }}>
      <ImageBackground
        style={directorsStyles.image}
        source={
          error
            ? require('../../assets/images/tommy.png')
            : {uri: director.photo}
        }>
        <Text style={directorsStyles.name} numberOfLines={2}>
          {director.name}
        </Text>
      </ImageBackground>
      <Text style={directorsStyles.biography}>{director.biography}</Text>
    </TouchableOpacity>
  );
};

export default DirectorComponent;
