import {StyleSheet, Dimensions} from 'react-native';

const {height} = Dimensions.get('window');
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    color: '#000',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  reminderText: {
    fontSize: 20,
    alignItems: 'flex-start',
    marginTop: 28,
    color: '#c4c4c4',
  },

  mainText: {
    fontSize: 37,
    marginTop: 12,
    color: '#000',
  },

  solutionText: {
    width: width * 0.56,
    textAlign: 'center',
    fontSize: 31,
    marginTop: 30,
    color: '#000',
    backgroundColor: '#D2EFFF',
    paddingVertical: 30,
    borderRadius: 15,
  },

  AContainer: {
    flex: 1,
    flexDirection: 'column',
  },

  AText: {
    width: width * 0.56,
    textAlign: 'center',
    fontSize: 31,
    marginTop: 30,
    paddingHorizontal: 30,
    color: '#000',
    backgroundColor: '#D2EFFF',
    paddingVertical: 30,
    borderRadius: 15,
  },

  DirectorImage: {
    width: 250,
    height: 175,
  },
});
