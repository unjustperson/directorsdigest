const colors = {
  alfredo: '#FFFECF',
  reddish: '#8E3C26',
  darkBrown: '#5E3F3D',
  orange: '#FFC10A',
  lightOrange: '#FFDC72',
  black: '#000000'
};

export default colors;
