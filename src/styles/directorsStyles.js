import {StyleSheet, Dimensions} from 'react-native';
import colors from './colors';

const {height} = Dimensions.get('window');
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  screen: {
    flex: 1,
    color: colors.black,
    backgroundColor: colors.alfredo,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  title: {
    fontFamily: 'Raleway-semibold',
    alignSelf: 'flex-start',
    fontSize: 36,
    padding: 20,
    color: colors.reddish,
  },

  flatList: {
    marginHorizontal: 35,
    paddingStart: 35,
    paddingEnd: 35,
    marginEnd: 35,
  },

  container: {
    width: width * 0.7,
    marginEnd: 25,
  },

  name: {
    fontFamily: 'Raleway-semibold',
    fontSize: 31,
    color: colors.orange,
    alignSelf: 'flex-end',
    position: 'absolute',
    paddingEnd: 22,
    marginBottom: 15,
    bottom: 0,
    end: 0,
  },

  biography: {
    fontFamily: 'Raleway-medium',
    color: colors.darkBrown,
    marginTop: 15,
    width: width * 0.7,
    fontSize: 16,
  },
  image: {
    borderRadius: 30,
    overflow: 'hidden',
    width: width * 0.7,
    height: height * 0.4,
  },

  directorFilmographyName: {
    fontFamily: 'Raleway-medium',
    alignSelf: 'center',
    fontSize: 36,
    paddingTop: 2,
    color: colors.orange,
  },

  filmographyText: {
    fontFamily: 'Raleway-medium',
    color: colors.darkBrown,
    paddingBottom: 12,
    fontSize: 16,
  },

  bottomNav: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: colors.alfredo,
    borderTopColor: colors.darkBrown,
    borderTopWidth: 1,
    height: 71,
    width: width,
  },
  bottomNavElement: {
    fontSize: 16,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: 'Raleway-medium',
    color: colors.darkBrown,
    flexDirection: 'column',
  },
  bottomNavIcon: {
    alignSelf: 'center',
  },
  bottomNavText: {
    fontSize: 16,
    marginTop: -3,
    fontFamily: 'Raleway-medium',
    color: colors.darkBrown,
  },
  activeElement: {
    fontSize: 17,
    fontFamily: 'Raleway-medium',
    color: colors.reddish,
  },

  filmsScreenTitle: {
    alignSelf: 'center',
    fontFamily: 'Raleway-semibold',
    fontSize: 31,
    padding: 20,
    color: colors.reddish,
  },
});
