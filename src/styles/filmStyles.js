import {StyleSheet, Dimensions} from 'react-native';
import colors from './colors';

const {height} = Dimensions.get('window');
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  flatList: {
    justifyContent: 'center',
    alignSelf: 'center',
  },

  filmographyFlatList: {
    justifyContent: 'center',
    alignSelf: 'center',
  },

  container: {
    width: width * 0.8,
    paddingBottom: 16,
  },

  image: {
    borderRadius: 15,
    overflow: 'hidden',
    resizeMode: 'contain',
    width: width * 0.8,
    height: height * 0.2,
  },

  logoImage: {
    width: width * 0.31,
    height: height * 0.06,
    marginRight: 9,
    marginBottom: 4,
    alignSelf: 'flex-end',
    position: 'absolute',
    bottom: 0,
    end: 0,
  },

  filmDetailPhoto: {
    width: width,
    height: height * 0.33,
  },

  filmDetailTitle: {
    color: colors.orange,
    fontFamily: 'Raleway-medium',
    fontSize: 32,
    alignSelf: 'flex-start',
    marginStart: 24,
    marginTop: 12,
  },
  filmDetailYear: {
    fontSize: 32,
    color: colors.lightOrange,
    fontFamily: 'Raleway-medium',
  },
  filmDetailSynopsis: {
    fontSize: 14,
    color: colors.darkBrown,
    alignSelf: 'flex-start',
    fontFamily: 'Raleway-medium',
    marginStart: 24,
    marginTop: 28,
    marginEnd: 40,
  },
  filmDetailWebView: {
    flex: 0,
    alignSelf: 'flex-start',
    height: height * 0.3,
    width: width * 0.9,
    marginStart: 24,
    marginEnd: 24,
    marginTop: 12,
    marginBottom: 0,
  },
  filmDetailDirectorPlaceholder: {
    fontSize: 14,
    fontFamily: 'Raleway-medium',
    color: colors.reddish,
    alignSelf: 'flex-start',
    marginStart: 24,
    marginTop: 0,
  },
  filmDetailDirector: {
    marginTop: 0,
    fontSize: 14,
    fontFamily: 'Raleway-medium',
    color: colors.darkBrown,
  },

  backButtonFilmography: {
    alignSelf: 'flex-start',
    marginTop: 20,
    marginStart: 30,
  },
});
