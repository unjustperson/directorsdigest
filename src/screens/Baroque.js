/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Text, View, ScrollView} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import styles from '../styles/styles';

export default function Baroque() {
  const firstA = ['Appricot', 'Ansamble', 'Accord', 'Advice', 'Allocate'];
  const secondA = ['Akron', 'Avala', 'Augur', 'Advent', 'Arilje'];

  const [data, setData] = useState(firstA);

  return (
    <View style={styles.container}>
      <Text style={styles.reminderText}>Data change on press</Text>
      <Text style={styles.mainText}>Baroque</Text>
      <BaroqueContainer
        activeDataValues={data}
        dataValues={[firstA, secondA]}
        setDataValues={setData}
      />
    </View>
  );
}

const BaroqueContainer = ({activeDataValues, dataValues, setDataValues}) => (
  <ScrollView style={[styles.AContainer]}>
    <TouchableOpacity
      onPress={() => {
        setDataValues(dataValues[1]);
        if (dataValues[1].toString() === activeDataValues.toString()) {
          setDataValues(dataValues[0]);
        }
      }}
      activeOpacity={0.4}>
      <Text style={[styles.AText, {backgroundColor: '#FFDAD0'}]}>
        {activeDataValues[0]}
        {console.log(activeDataValues)}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#FFAE97'}]}>
        {activeDataValues[1]}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#FF8A68'}]}>
        {activeDataValues[2]}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#D87A5F'}]}>
        {activeDataValues[3]}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#B35134'}]}>
        {activeDataValues[4]}
      </Text>
    </TouchableOpacity>
  </ScrollView>
);
