/* eslint-disable no-undef */
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Text, View} from 'react-native';
import {FlatList, TextInput} from 'react-native-gesture-handler';

import styles from '../styles/styles';

function mapListItems(items) {
  return items.map((v, i) => ({key: i.toString(), v}));
}

function filterAndSort(data, text, asc) {
  return data
    .filter(i => text.length === 0)
    .sort(
      asc
        ? (a, b) => (b > a ? -1 : a === b ? 0 : 1)
        : (a, b) => (a > b ? -1 : a === b ? 0 : 1),
    );
}

function fetchItems(filter, asc) {
  return new Promise(resolve => {
    resolve({
      json: () =>
        Promise.resolve({
          items: filterAndSort(items, filter, asc),
        }),
    });
  });
}

export default DalvikListContainer = () => {
  const [asc, setAsc] = useState(true);
  const [filter, setFilter] = useState('');
  const [data, setData] = useState(
    filterAndSort(
      new Array(100).fill(null).map((v, i) => `Dalvik ${i + 1}`),
      '',
    ),
  );

  return (
    <View style={styles.container}>
      <Text style={styles.reminderText}>React Native</Text>
      <Text style={styles.mainText}>Dalvik</Text>
      <DalvikList
        data={mapListItems(data)}
        asc={asc}
        onFilter={text => {
          setFilter(text);
          setData(filterAndSort(data, text, asc));
        }}
        onSort={() => {
          setAsc(!asc);
          setData(filterAndSort(data, filter, asc));
        }}
      />
      {console.log(data)}
    </View>
  );
};

function DalvikList({Controls, data, onFilter, onSort, asc}) {
  return (
    <FlatList
      data={data}
      ListHeaderComponent={<Controls {...{onFilter, onSort, asc}} />}
      renderItem={({item}) => (
        <Text style={styles.AText}>
          {''}
          {item.v}
          {console.log(item.v)}
        </Text>
      )}
    />
  );
}

DalvikList.propTypes = {
  Controls: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  onFilter: PropTypes.func.isRequired,
  onSort: PropTypes.func.isRequired,
  asc: PropTypes.bool.isRequired,
};

DalvikList.defaultProps = {
  Controls: DalvikListControls,
};

function DalvikListControls({onFilter, onSort, asc}) {
  return (
    <View>
      <DalvikListFilter onFilter={onFilter} />
      <DalvikListSort onSort={onSort} asc={asc} />
    </View>
  );
}

DalvikListControls.propTypes = {
  onFilter: PropTypes.func.isRequired,
  onSort: PropTypes.func.isRequired,
  asc: PropTypes.bool.isRequired,
};

function DalvikListFilter({onFilter}) {
  return (
    <View>
      <TextInput autoFocus placeholder="Search" onChangeText={onFilter} />
    </View>
  );
}

const arrows = new Map([
  [true, '▼'],
  [false, '▲'],
]);

function DalvikListSort({onSort, asc}) {
  return (
    <Text onPress={onSort}>
      {arrows.get(asc)}
      {console.log(asc)}
    </Text>
  );
}

DalvikListSort.propTypes = {
  onSort: PropTypes.func.isRequired,
  asc: PropTypes.bool.isRequired,
};
