/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-undef */
import React, {useState} from 'react';
import {Text, View, ScrollView} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import styles from '../styles/styles';

export default Alphabet = () => {
  const [direction, setDirection] = useState('column');
  return (
    <View style={styles.container}>
      <Text style={styles.reminderText}>Row and column change</Text>
      <Text style={styles.mainText}>Alphabet</Text>
      <AlphabetContainer
        selectedValue={direction}
        values={['column', 'row']}
        setSelectedValue={setDirection}
      />
    </View>
  );
};

const blockValues = ['Appricot', 'Ansamble', 'Accord', 'Advice', 'Allocate'];

const AlphabetContainer = ({selectedValue, values, setSelectedValue}) => (
  <ScrollView
    style={[styles.AContainer]}
    showsHorizontalScrollIndicator={true}
    horizontal={selectedValue === values[1] ? true : false}>
    <TouchableOpacity
      onPress={() => {
        setSelectedValue(values[1]);
        if (selectedValue === values[1]) {
          setSelectedValue(values[0]);
        }
      }}
      activeOpacity={0.4}
      style={[
        {flexDirection: selectedValue},
        selectedValue === values[1] ? {justifyContent: 'space-evenly'} : {},
      ]}>
      <Text style={styles.AText}>{blockValues[0]}</Text>
      <Text style={[styles.AText, {backgroundColor: '#9DDDFF'}]}>
        {blockValues[1]}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#6FCBFD'}]}>
        {blockValues[2]}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#71CDFF'}]}>
        {blockValues[3]}
      </Text>
      <Text style={[styles.AText, {backgroundColor: '#47BBFA'}]}>
        {blockValues[4]}
      </Text>
    </TouchableOpacity>
  </ScrollView>
);
