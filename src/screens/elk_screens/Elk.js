/* eslint-disable no-undef */
import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {SvgXml} from 'react-native-svg';
import Directors from './Directors';
import Film from './Film';
import directorsStyles from '../../styles/directorsStyles';
import directorXml from '../../util/directorXml';
import filmXml from '../../util/filmXml';

export default Elk = () => {
  const [screen, setScreen] = useState('Directors');
  const screens = ['Directors', 'Films'];

  /*
  const [subScreen, setSubScreen] = useState('Directors');
  const subScreens = ['Directors', 'DirectorFilmography', 'FilmDetail']; */

  /*   function switchSubScreen({subScreen, subScreens}) {
    switch (subScreen) {
      case subScreen === subScreens[0]:
        return <Directors />;
    }
  }

  console.log(subScreen === subScreens[0]); */

  return (
    <>
      {screen === screens[0] ? <Directors /> : <Film />}
      <DigestBottomNav
        active={screen}
        labelValues={['Directors', 'Films']}
        setActive={setScreen}
      />
    </>
  );
};

const DigestBottomNav = ({active, setActive, labelValues}) => {
  return (
    <View style={directorsStyles.bottomNav}>
      <View style={directorsStyles.bottomNavElement}>
        <SvgXml
          width={40}
          height={40}
          onPress={() => {
            setActive(labelValues[0]);
          }}
          style={[
            directorsStyles.bottomNavIcon,
            active === labelValues[0]
              ? directorXml.replace('none', '#8E3C26')
              : {},
          ]}
          xml={directorXml}
        />
        <Text
          style={[
            directorsStyles.bottomNavText,
            active === labelValues[0] ? directorsStyles.activeElement : {},
          ]}
          onPress={() => {
            setActive(labelValues[0]);
          }}>
          Directors
        </Text>
      </View>

      <View style={directorsStyles.bottomNavElement}>
        <SvgXml
          width={40}
          height={40}
          onPress={() => {
            setActive(labelValues[1]);
          }}
          style={[
            directorsStyles.bottomNavIcon,
            active === labelValues[1]
              ? filmXml.replace('5E3F3D', '8E3C26')
              : {},
          ]}
          xml={filmXml}
        />
        <Text
          style={[
            directorsStyles.bottomNavText,
            active === labelValues[1] ? directorsStyles.activeElement : {},
          ]}
          onPress={() => {
            setActive(labelValues[1]);
          }}>
          Films
        </Text>
      </View>
    </View>
  );
};
