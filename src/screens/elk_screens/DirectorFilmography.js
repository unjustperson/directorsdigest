/* eslint-disable no-undef */
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Text, ImageBackground, Image} from 'react-native';
import {SvgXml} from 'react-native-svg';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import directorsStyles from '../../styles/directorsStyles';
import filmStyles from '../../styles/filmStyles';
import {SafeAreaView} from 'react-native-safe-area-context';
import backXml from '../../util/backXml';
import FilmComponent from '../../components/FilmComponent';
import RoomArray from '../../util/RoomArray';

export default DirectorFilmography = ({route}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const loadRoom = 'loadRoom';
  const filledRoom = RoomArray;

  const getFilms = async () => {
    try {
      const response = await fetch('https://govorimo.com/films', {
        method: 'GET',
        headers: {
          Authorization: 'Token 7656ed7b75ba26e9a8abea8f22c2bc3d01478300',
        },
      });
      const json = await response.json();
      setData(json.results);
    } catch (error) {
      setData(loadRoom);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getFilms();
  }, []);

  const navigation = useNavigation();

  return (
    <SafeAreaView style={directorsStyles.screen}>
      <SvgXml
        width={40}
        height={40}
        style={filmStyles.backButtonFilmography}
        xml={backXml}
        onPress={() => {
          navigation.goBack();
        }}
      />
      <Text style={directorsStyles.directorFilmographyName}>
        {route.params.director.name}
      </Text>
      <Text style={directorsStyles.filmographyText}>Filmography</Text>
      {isLoading ? (
        <ActivityIndicator />
      ) : data === loadRoom ? (
        <FlatList
          contentContainerStyle={filmStyles.flatList}
          data={filledRoom}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => <FilmComponent film={item} error={true} />}
        />
      ) : (
        <FlatList
          contentContainerStyle={filmStyles.flatList}
          data={data.filter(it => it.director === route.params.director.name)}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => <FilmComponent film={item} error={false} />}
        />
      )}
    </SafeAreaView>
  );
};
