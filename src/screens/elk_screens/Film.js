/* eslint-disable no-undef */
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import directorsStyles from '../../styles/directorsStyles';
import filmStyles from '../../styles/filmStyles';
import RoomArray from '../../util/RoomArray';
import FilmComponent from '../../components/FilmComponent';

const filledRoom = RoomArray;

export default Film = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const loadRoom = 'loadRoom';

  const getFilms = async () => {
    try {
      const response = await fetch('https://govorimo.com/films', {
        method: 'GET',
        headers: {
          Authorization: 'Token 7656ed7b75ba26e9a8abea8f22c2bc3d01478300',
        },
      });
      const json = await response.json();
      setData(json.results);
    } catch (error) {
      setData(loadRoom);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getFilms();
  }, []);

  return (
    <SafeAreaView style={directorsStyles.screen}>
      <Text style={directorsStyles.filmsScreenTitle}>Films</Text>
      {isLoading ? (
        <ActivityIndicator />
      ) : data === loadRoom ? (
        <FlatList
          contentContainerStyle={filmStyles.flatList}
          data={filledRoom}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => <FilmComponent film={item} error={true} />}
        />
      ) : (
        <FlatList
          contentContainerStyle={filmStyles.flatList}
          data={data}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => <FilmComponent film={item} error={false} />}
        />
      )}
    </SafeAreaView>
  );
};
