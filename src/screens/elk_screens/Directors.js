/* eslint-disable no-undef */
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Text, ImageBackground} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import directorsStyles from '../../styles/directorsStyles';
import {SafeAreaView} from 'react-native-safe-area-context';
import TommyArray from '../../util/TommyArray';
import DirectorComponent from '../../components/DirectorComponent';

const filledTommy = TommyArray;

export default Directors = ({subScreen}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const loadTommy = 'LoadTommy';

  const getDirectors = async () => {
    try {
      const response = await fetch('https://govorimo.com/directors', {
        method: 'GET',
        headers: {
          Authorization: 'Token 7656ed7b75ba26e9a8abea8f22c2bc3d01478300',
        },
      });
      const json = await response.json();
      setData(json.results);
    } catch (error) {
      setData(loadTommy);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getDirectors();
  }, []);

  return (
    <SafeAreaView style={directorsStyles.screen}>
      <Text style={directorsStyles.title}>Directors Digest</Text>
      {isLoading ? (
        <ActivityIndicator />
      ) : data === loadTommy ? (
        <FlatList
          contentContainerStyle={directorsStyles.flatList}
          horizontal
          data={filledTommy}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => (
            <DirectorComponent director={item} error={true} />
          )}
        />
      ) : (
        <FlatList
          contentContainerStyle={directorsStyles.flatList}
          horizontal
          data={data}
          keyExtractor={({id}, index) => id}
          renderItem={({item}) => (
            <DirectorComponent director={item} error={false} />
          )}
        />
      )}
    </SafeAreaView>
  );
};
