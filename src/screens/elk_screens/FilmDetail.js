/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-undef */
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Text, ImageBackground} from 'react-native';
import WebView from 'react-native-webview';
import directorsStyles from '../../styles/directorsStyles';
import filmStyles from '../../styles/filmStyles';
import {SvgXml} from 'react-native-svg';
import {SafeAreaView} from 'react-native-safe-area-context';
import backXml from '../../util/backXml';

export default FilmDetail = ({route}) => {
  const film = route.params.film;

  const navigation = useNavigation();

  return (
    <SafeAreaView style={directorsStyles.screen}>
      <ImageBackground
        style={filmStyles.filmDetailPhoto}
        source={{uri: film.film_photo}}>
        <SvgXml
          width={40}
          height={40}
          style={filmStyles.backButtonFilmography}
          xml={backXml}
          onPress={() => {
            navigation.goBack();
          }}
        />
      </ImageBackground>
      <Text style={filmStyles.filmDetailTitle}>
        {film.film_name}
        <Text style={filmStyles.filmDetailYear}> ({film.year})</Text>
      </Text>
      <Text style={filmStyles.filmDetailSynopsis}>{film.film_synopsis}</Text>
      <WebView
        style={filmStyles.filmDetailWebView}
        javaScriptEnabled={true}
        source={{
          uri: `https://www.dailymotion.com/embed/video/${film.trailer_link}?autoplay=1`,
        }}
      />
      <Text
        style={[filmStyles.filmDetailDirectorPlaceholder, {paddingBottom: 12}]}>
        Director:{' '}
        <Text style={filmStyles.filmDetailDirector}>{film.director}</Text>
      </Text>
      <Text
        style={[filmStyles.filmDetailDirectorPlaceholder, {paddingBottom: 24}]}>
        Screenplay:{' '}
        <Text style={filmStyles.filmDetailDirector}>{film.screenplay}</Text>
      </Text>
    </SafeAreaView>
  );
};
