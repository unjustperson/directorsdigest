/* eslint-disable no-undef */
import React from 'react';
import {Text, View} from 'react-native';

import styles from '../styles/styles';

export default Celsius = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.reminderText}>React Native</Text>
      <Text style={styles.mainText}>Celsius</Text>
      <Text
        style={styles.solutionText}
        onPress={() => navigation.navigate('Baroque')}>
        {route.params.blockValue}
      </Text>
    </View>
  );
};
