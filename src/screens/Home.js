/* eslint-disable no-undef */
import React from 'react';
import {Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import styles from '../styles/styles';

export default Home = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.reminderText}>React Native</Text>
      <Text style={styles.mainText}>Knowledge book</Text>
      <ScrollView styles={styles.AContainer}>
        <Text
          style={styles.solutionText}
          onPress={() => navigation.navigate('Alphabet')}>
          Alphabet
        </Text>
        <Text
          style={styles.solutionText}
          onPress={() => navigation.navigate('Baroque')}>
          Baroque
        </Text>
        <Text
          style={styles.solutionText}
          onPress={() =>
            navigation.navigate('Celsius', {blockValue: 'Cellar'})
          }>
          Celsius
        </Text>
        <Text
          style={styles.solutionText}
          onPress={() => navigation.navigate('Dalvik')}>
          Dalvik
        </Text>
        <Text
          style={styles.solutionText}
          onPress={() => navigation.navigate('Elk')}>
          Elk
        </Text>
        <Text
          style={styles.solutionText}
          onPress={() => navigation.navigate('Fun')}>
          Fun
        </Text>
      </ScrollView>
    </View>
  );
};
