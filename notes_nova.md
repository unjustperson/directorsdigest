## Verzije
React-DOM - 17.0.2
React-native - 0.65.1 (aug 2021.)
npm - 7.21.1

## moje
React-DOM - 17.0.2
React-native 0.67.2 (jan 2022.)


## Remember
nova - struktura projekta
- __ mocks__
- .vscode
- android
- build
- config
- fastlane
- flow
- ios
- readme 
- release-notes
- src

### __ mocks__

### .vscode

Ima settings.json file koji konfigurise settings za vscode kada se otvori nova folder

### android

Android projekat

### build/huawei

Jedini path u build je ovaj. Sadrzi .jar verovatno za buildovanje apka za AppGallery

### config

Folderi
- N1
- Nova
- SK

svi sadrze sledece fajlove:
- .env.common
- .env.dev
- .env.prod
- .env.qa
- .env.stage

### fastlane

- AuthKey.p8
- Fastfile.rb
- Pluginfile.txt
- Readme.md

### flow

- bundle
- code
- codepush
- constans
- debug
- prompts
- publish

### ios

Ios projekat

### readme
.png-ovi prezentacioni

### release-notes
- N1 
- Nova
- SK

svaki sadrzi

- dev.txt
- prod.txt
- qa.txt
- stage.txt

### src 
source kod aplikacije

4.mar 2022.

imena foldere(LOC):
- assets (3230)
- components (14375)
- containers (8410)
- i18n (1215)
- redux (90)
- theme (13725)
- utils (545)


## TODO LEARN
redux-saga
redux-flipper
redux-actions