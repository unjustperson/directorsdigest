## REMEMBER
equals when comparing in arrays in JS doesn't cut it, you need to either
compare them as strings or compare their elements in iteration


JS actually does JIT compilation at runtime but it's still considered an interpreted language as JIT compilation is not done ahead of time

DOM je jedan od interfejsa koji pretrazivac ima u sebi, on razradjuje HTML, CSS i JS kod, reaguje na dogadjaje...

Pretrazivac izvrsava kod u izvrsnom okruzenju koje je za njega tab iliti kartica

{} u JS se koristi i za evaluate-ovanje JS izraza tokom kompilacije
JS izraz moze biti funkcija, objekat ili bilo kakav kod koji se pretvara u vrednost

Signature React Native funkcionalne komponente 
() => JSX.Element

({value1, value2}: {value1: any; value2:number;}) => JSX.Element

Nizovi u JS se potpisuju kao string[], number[], any[], 
(string | number)[]
any[][] (ovo je niz nizova)
any[][][]

index.js je entry point za rn aplikaciju gde AppRegistry poziva
funkciju registerComponent(appKey: string, ComponentProvider): string
ComponentProvider je nasa root React komponenta uglavnom je to App.js  () => JSX.Element
native sistem onda loaduje bundle za aplikaciju i kada je spremna da je pokrene sa AppRegistry.runApplication()
ComponentProvider tip je ustvari () => React.ComponentType<<any>any> a Reacy.ComponentType<<any>any> je ustvari ComponentClass<<P>P> ili FunctionComponent<<P>P>


```JSX
//defines an object with a property of color to a variable named blueColor
const blueColor = {color: 'blue'}
//{} are also used for evaluating values at compile time
<Text style={blueColor}>
<Text style={{color:'blue'}}>

```

array deconstructing, dekonstrukcija nizova se koristi u useState hooku na primer, 

```JSX
const [fruit, setFruit] = useState('orange');
```

je isto kao

```JSX
var fruitStateVariable = useState('orange'); // returns a Pair
var fruit = fruitStateVariable[0];
var setFruit = fruitStateVariable[1];
```

podaci koji se prosledjuju kroz navigaciju kao argumenti se zovu route parameters - parametri ruta

Klasicni primeri sideEfektova su na primer rucno menjanje DOM-a 
unutar React komponente
```JSX
useEffect(()=>{
    document.title = `Welcome ${user.name}`;
})
```
sideEfekti mogu biti i fetchovanje podataka, potpisivanje na neki entitet itd

React.createElement() vraca React element zadatog tipa <<div>div> <<img>img> ili kog vec u smislu html taga, 
tip koji se unosi moze biti i React funkcionalna ili klasna komponenta ili React fragment tip
Kada koristimo JSX mi ne pozivamo rucno ovu funkciju vec se ona automatski poziva.
Postoji i React.createFactory(type) funkcija koja kao factory funkcija proizvodi elemente odredjenog tipa i poziva se automatski ako koristimo JSX. Cak je i ne bi trebalo rucno pozivati za razliku od React.createElement() koga mozemo


Primer JS za RN modula
```JSX
const myApp = () => {
    return (
        <View style={{justifyContent: center, alignItems: center}}>
            <Text>My App</Text>
        </View>
    );
}

export default myApp;
```

modul ima default property koji eksportujemo i koji se zove myApp. Taj property je ustvari funkcija koja ima potpis () => JSX.Element  



Pre su samo klasne komponente imali properties a od dodavanja hukova mogu da ih imaju i funkcionalne komponente

Glavne komponente React sistema:
- komponente
- JSX
- props
- states

Na Androidu se razmsetaj definise preko ViewGroupa kao LinearLayout, RelativeLayout, ConstraintLayout dok se u RN ViewGroup poziva sa <<View>View> a razmestaj njegove dece preko Flexboxa

Sa props mogu da se dodaju osobine RN komponentama
```JSX
const myApp = (props) => {
    return (
        <View>
            <Text>{props.directorName} directed {props.directorMagnumOpus}
        </View>
    );
}

export default myApp;
```

Properties se RN komponenti prenose kao parametri funkcije
Postoje i ugradjeni Properties za komponente koji dozvoljavaju da se daju osobenosti tim komponentama koje se odrazvaju pri njihovom renderovanju
```JSX
<Image source={{uri: "https://reactnative.dev/docs/assets/p_cat1.png"}}>
```

primer sa useState()
```JSX
const Cat = (props) => {
    const [isHungry, setIsHungry] = useState(true);

    return(
        <View>
            <Text>
            I am {props.name} and I am {isHungry ? 'hungry' : 'full'}
            </Text>
            <Button
                onPress={() => {
                    setIsHungry(false)
                }}
                disabled={!isHungry}
                title={isHungry ? 'Some cool food pls' : 'You gucci...For now'} />
        </View>
    )
}

const Cafe = () => {
    return(
        <>
            <Cat name='Simba'>
        </>
    )
}
```
u zavisnosti od stanja imamo razlicite funkcionalnosti komponente, a stanje u ovom slucaju menjamo pritiskom na dugme
mozemo razmisljati ovako
[<<getter>getter>, <<setter>setter>] = useState(<<initialValue>initialValue>).

Kada koristimo <<>Fragment> u ReactWeb mozemo da renderujemo bez div-a a na RN mozemo bez <<View>View>

Jedan od glavnih property-ja <<TextInput>TextInput> je onChangeText koji poziva Callback kada dodje do promene texta

Required property za <<FlatList>FlatList> su data i renderItem

Metro bundler je servis koji builduje aplikacije za RN
Radi na portu 8081

Pristup Platform posebnim funkcijama se moze omoguciti preko Platform modula

asignSelf 'flex-start' stavlja komponentu na start, slicno rade i ostali atributi za asignSelf

ES5 je 2009. izaso i prva je velika promena javascripta
ES6 je 2015. izaso

ES6 - klase, const/let, array deconstructing, Promiseovi, arrow funkcije, Generatori...

React v17.0 izasao 2020. izbacene lifecycle metode componentWillMount, componentWillReceiveProps i componentWillUpdate

Da bi React Navigation radio potrebno je da se ceo App oblozi sa NavigationContainer
Stack.Navigator kontrolise decu odnosi Stack.Screenove ili Stack.Group
native-stack navigator koristi UINavigationController za ios i Fragmente za Android

bundle.js se nalazi na localhost:8081
yarn android-shake je komanda za pristup devtools, pre toga je potrebno u package.json ubaciti     
"android-shake": "adb shell input keyevent 82"
u "scripts"
testovanje performansi treba da se radi u release buildovima
jsthread je thread gde se izvrsava biznis logika 
main thread je UI thread gde se renderuje zadati UI

Gradle - build system, dependency management
Cocoapods - dependency management

rubygems.org je biblioteka ruby gemova
Bundle samo instalira one gemove koji su u gemfile.lock

Metro je packager i bundler 

Redux je state container za JS appove

Action je JS object koji ima type polje
Action je objekat koji opisuje nesto sto se desilo u aplikaciji
Dodatna polja u actionu koja opisuju sta se desilo se mogu staviti u polje payload
type uglavnom ima formu 'domain/eventName'

```JS
const addToDoAction = {
    type: 'todos/todoAdd'.
    payload: 'Learn Redux'
}
```

Action creator je funkcija koja vraca action objekat
```JS
const addTodoInstance = todoText => {
    return {
        type: 'todos/todoAdd',
        payload: todoText,
    }
}
```
Reduceri se tako zovu zbog slicnosti sa Array.reduce() funkcijom
Reducer je funkcija koja dobija state i action objekat i updateuje stanje i vraca novo stanje (state, action) => newState
Reduceri ne menjaju stanje vec kopiraju staro, dodaju promene i stvaraju novo

package.json struktura
- name
- version
- private
- scripts
- dependencies
- devDependencies

debug.keystore je potreban u android:app da bi se build task validateSigningDebug ispunio i aplikacija buildovala 
```
You can generate the debug keystore by running this command in the android/app/ directory: keytool -genkey -v -keystore debug.keystore
  -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000
```

Ako ne kreiramo varijablu sa let, const ili var onda se stvorena variabla stvara kao property Globalnog objekta

Array.slice() ako ima samo prvi parametar u novi Array koji vraca cuva od datog indexa pa do kraja i te elemente brise iz Arraya na koji je pozvan

```JS
const months = ['jan', 'feb', 'mar', 'apr', 'may'];
months.splice(2) // ovo vraca Array(3) ['mar', 'apr', 'may']
months // vraca Array(2) ['jan', 'feb']
```

Ako se doda drugi parametar, kazuje se koliko sledeci elemenata od startIndexa zelimo da spliceujemo

```JS
const months = ['jan', 'feb', 'mar', 'apr', 'may'];
months.splice(2, 2) // ovo vraca Array(2) ['mar', 'apr']
months // vraca Array(3) ['jan', 'feb', 'may']
```


createStore() vraca StoreCreator interface koji je ustvari funkcija, ima 2 potpisa, za jedan je potreban reducer i enhancer a drugi reducer, preloadedState i enhancer. Oba vracaju Store interface koji ima svoje funkcionalnosti kao dispatch, getState(), subscribe(listener: ()=>void): Unsubscribe
replaceReducer(nextReducer: Reducer): void

SonarQube
static code analysis uglavnom se kao servis ubacuje u deploy pipeline

issues: 
minor major uglavnom mogu da prodju build
critical i blocker obaraju build

jenkins pipeline:
- checkout
- dependency check
- unit tests
- quality gate

za nove projekte i feature stavlja se minimal test coverage u procentima npr 80% i projekat ne moze da se builduje ako nema taj procenat

funckija sa previse parametra  moze da se resi sa Buildere patternom

### MySocialNetwork app

friendsReducer prima inicijalno stanje i vraca novo stanje objekta stanje. Objekat stanje ima 2 liste - current i possible.
U inicijalnom stanju, svi prijatelji su u possible.
Kada se desi akcija tipa ADD_FRIEND dekonstruiramo state objekat sa lokalnim current i possible instancama, splajsujemo possible prijatelje u odnosu na payload koji nam je poslat iz View-a putem Action objekta - koji sadrzi type i payload polje

### TODO LEARN
Javascript interfaces and classes

Progressive i interlaced video 

### TODO
- svg fill:none promeni u directorXml
### PIDs
- Accountability app with calendars and comments, milestones...
- Action: proofOfDone?, daysPositive, daysNegative, streak...

## Directors Digest
### Elk 
- JSX.Element odnosto React funkcionalna komponenta koja renderuje aplikaciju
- ima 2 stanja 'Directors' i 'Films' u odnosu na koje se renderuje odgovarajuci screen
- setovanje stanja se obavllja kroz komponentu DigestBottomNav koja kao property uzima prometu stanja ekrana  
- setActive = {setScreen}
- pritiskom na element u DigestBotomNav se menja njegov property setActive, a setActive property ima vrednost setScreen pa se menja screenState i renderuje jedan ili drugi screen u odnosu na state

### Directors
- ima 2 stanja koja se kontrolisu isLoading i data
- data se setuje kroz network request json.results ako je uspesan zahtev a default placeholder data se setuje ako je doslo do greske u zahtevu



